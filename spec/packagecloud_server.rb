# encoding: utf-8
require 'webrick'

class PackagecloudServer < WEBrick::HTTPServlet::AbstractServlet
  DISTRIBUTIONS = File.read("spec/fixtures/distros.json")
  PACKAGE_CONTENTS = '{"files":[{"filename":"jake_1.0.orig.tar.bz2","size":1108,"md5sum":"a7a309b55424198ee98abcb8092d7be0"},{"filename":"jake_1.0-7.debian.tar.gz","size":1571,"md5sum":"0fa5395e95ddf846b419e96575ce8044"}]}'
  GEM_VERSION = '{"major":"0","minor":"2","patch":"1"}'
  REPO = '{"name": "test_repo","created_at": "2014-08-30T03:51:37.000Z","url": "https://packagecloud.io/joedamato/test_repo","last_push_human": "about 2 months ago","package_count_human": "4 packages","private": true,"fqname": "joedamato/test_repo"}'
  REPOS = "[#{REPO}]"
  PACKAGES = '[{"name":"gitlab-ce","distro_version":"ubuntu/xenial","created_at":"2017-01-21T23:50:27.000Z","version":"8.15.5","release":"ce.0","epoch": 0,"private":true,"type":"deb","filename":"gitlab-ce_8.15.5-ce.0_amd64.deb","uploader_name":"joedamato","indexed":true,"repository_html_url":"/joedamato/test_repo","package_url":"/api/v1/repos/joedamato/test_repo/package/deb/ubuntu/xenial/gitlab-ce/amd64/8.15.5-ce.0.json","downloads_detail_url":"/api/v1/repos/joedamato/test_repo/package/deb/ubuntu/xenial/gitlab-ce/amd64/8.15.5-ce.0/stats/downloads/detail.json","downloads_series_url": "/api/v1/repos/joedamato/test_repo/package/deb/ubuntu/xenial/gitlab-ce/amd64/8.15.5-ce.0/stats/downloads/series/daily.json","downloads_count_url": "/api/v1/repos/joedamato/test_repo/package/deb/ubuntu/xenial/gitlab-ce/amd64/8.15.5-ce.0/stats/downloads/count.json","package_html_url": "/joedamato/test_repo/packages/ubuntu/xenial/gitlab-ce_8.15.5-ce.0_amd64.deb","promote_url":"/api/v1/repos/joedamato/test_repo/ubuntu/xenial/gitlab-ce_8.15.5-ce.0_amd64.deb/promote.json"}]'
  PROMOTED = '{"name":"gitlab-ce","distro_version":"ubuntu/xenial","architecture":"amd64","repository":"public","size":"311509298","summary":null,"filename":"gitlab-ce_8.15.5-ce.0_amd64.deb","description":"GitLab Community Edition and GitLab CI (including NGINX, Postgres, Redis)","dependencies":[{"context":"depends","dependencies":["openssh-server"]}],"md5sum":"bd1427fbe923607536912d00e52c6a15","sha1sum":"b6acc61629026e33046122049213b29ecfc8c25a","sha256sum":"9cdcd68a5e7bb391a88d7279c9bf178aacba62c6827b9a0db0e4dd67604a33b5","sha512sum":"3b81d3e09fec4d0c4c97bfeccb4bb040b8c7b7913cc3aa7b6380ccd581f1eea08d605fbecf526f6e549e609242f4ba725d5ec8cdab01fe6ecbf3754e8cd93b84","private":false,"uploader_name":"joedamato","created_at":"2017-01-21T23:50:27.000Z","licenses":[],"version":"8.15.5","release":"ce.0","epoch":0,"indexed":false,"repository_url":"/api/v1/repos/joedamato/public","repository_html_url":"/joedamato/public","package_url":"/api/v1/repos/gabriel/public/package/deb/ubuntu/xenial/gitlab-ce/amd64/8.15.5-ce.0.json","versions_url":"/api/v1/repos/gabriel/public/package/deb/ubuntu/xenial/gitlab-ce/amd64/versions.json","promote_url":"/api/v1/repos/joedamato/test_repo/ubuntu/xenial/gitlab-ce_8.15.5-ce.0_amd64.deb/promote.json","total_downloads_count":0,"download_url":"https://packages.gitlab.com/joedamato/public/packages/ubuntu/xenial/gitlab-ce_8.15.5-ce.0_amd64.deb/download","package_html_url":"/joedamato/public/packages/ubuntu/xenial/gitlab-ce_8.15.5-ce.0_amd64.deb","downloads_detail_url":"/api/v1/repos/joedamato/public/package/deb/ubuntu/xenial/gitlab-ce/amd64/8.15.5-ce.0/stats/downloads/detail.json","downloads_series_url":"/api/v1/repos/joedamato/public/package/deb/ubuntu/xenial/gitlab-ce/amd64/8.15.5-ce.0/stats/downloads/series/daily.json","downloads_count_url":"/api/v1/repos/joedamato/public/package/deb/ubuntu/xenial/gitlab-ce/amd64/8.15.5-ce.0/stats/downloads/count.json","self_url":"/api/v1/repos/joedamato/public/package/deb/ubuntu/xenial/gitlab-ce/amd64/8.15.5-ce.0.json"}'

  def json_response(request, response, body)
    response.status = 200
    response['Content-Type'] = "application/json"
    response.body = body
    $request, $response = request, response
  end

  def plain_response(request, response, body)
    response.status = 200
    response['Content-Type'] = "text/plain"
    response.body = body
    $request, $response = request, response
  end

  def created_response(request, response)
    response.status = 201
    response['Content-Type'] = "text/plain"
    response.body = "{}"
    ### Cheap hack to avoid parsing a multipart request here,
    ### instead we just check that the body isn't impossibly small
    if (request.body.size < 1000)
      raise "Request is too small! #{request.body.size}"
    end
    $request, $response = request, response
  end

  def default_response(request, response)
    response.status = 404
    response['Content-Type'] = "text/plain"
    response.body = "nope"
    $request, $response = request, response
  end

  def forbidden_response(request, response)
    response.status = 401
    response['Content-Type'] = "text/plain"
    response.body = "{ 'error': 'unauthenticated' }"

    $request, $response = request, response
  end

  def route(request, response)
    if request["Authorization"] && request["Authorization"] != "Basic dGVzdF90b2tlbjo="
      return forbidden_response(request, response)
    end

    path = request.path
    case path
      when "/api/v1/distributions.json"
        json_response(request, response, DISTRIBUTIONS)
      when "/api/v1/repos.json"
        if request.request_method == "GET"
          json_response(request, response, REPOS)
        else
          plain_response(request, response, "{}")
        end
      when "/api/v1/repos/joedamato/test_repo.json"
        json_response(request, response, REPO)
      when "/api/v1/repos/joedamato/test_repo/packages.json"
        if request.request_method == "POST"
          created_response(request, response)
        elsif request.request_method == "GET"
          json_response(request, response, PACKAGES)
        end
      when "/api/v1/repos/joedamato/test_repo/ubuntu/xenial/gitlab-ce_8.15.5-ce.0_amd64.deb/promote.json"
        if request.request_method == "POST"
          if request.query.key? "destination"
            json_response(request, response, PROMOTED)
          end
        end
      when "/api/v1/repos/joedamato/test_repo/packages/contents.json"
        plain_response(request, response, PACKAGE_CONTENTS)
      when "/api/v1/gem_version.json"
        json_response(request, response, GEM_VERSION)
      else
        default_response(request, response)
        $request, $response = request, response
    end
  end

  def do_GET(request, response)
    route(request, response)
  end

  def do_POST(request, response)
    route(request, response)
  end
end
